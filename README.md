# Aplikasi Analisis sebaran COVID-19, Mengunkan metode K-medoid dan metode spektral di daerah Tasik

Dalam studi kasus sebaran COVID-19, aplikasi menggunakan Django untuk membandingkan metode k-medoids dan spectral clustering dengan menggunakan metrik Silhouette Score dan elbow method.

Pertama, metode k-medoids dan spectral clustering digunakan untuk mengelompokkan data sebaran COVID-19 berdasarkan atribut-atribut seperti lokasi geografis, jumlah kasus terkonfirmasi, dan tingkat penyebaran.

Selanjutnya, metrik Silhouette Score digunakan untuk mengevaluasi kualitas klastering yang dihasilkan oleh kedua metode. Nilai Silhouette Score yang lebih tinggi menunjukkan bahwa pengelompokan lebih baik, dengan setiap titik data lebih dekat ke anggota klaster yang sama dan lebih jauh dari klaster lainnya.

Selain itu, elbow method juga digunakan untuk menentukan jumlah klaster yang optimal dalam k-medoids dan spectral clustering. Elbow method mencari titik di mana penurunan dalam nilai cost (misalnya, inertia atau dispersion) mulai melambat secara signifikan, menandakan bahwa penambahan klaster selanjutnya tidak memberikan peningkatan yang signifikan dalam pemahaman struktur data.

Dengan menggunakan Django, analisis ini dapat diimplementasikan secara efisien dalam aplikasi web, memberikan insight yang berharga tentang pola sebaran COVID-19 dan membantu dalam pengambilan keputusan yang lebih baik dalam menangani pandemi ini.

### Note 
jika anda ingin mejalankanya aplikasi tolong hubingi saya [Muhammad Ramdhani](wa.me/+6281399057525) karena aplikasi ini ada data privasi yang tidak saya sertakan pada repository ini