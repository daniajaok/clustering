from django.apps import AppConfig


class SpectralConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'spectral'
