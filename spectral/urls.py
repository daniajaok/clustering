from django.urls import path
from . import views

app_name = 'spectral'
urlpatterns = [
    path('refresh-cluster', views.refresh_cluster, name='spectral_refresh'),
    path('cluster-0', views.spectral_cluster0, name='cluster_0'),
    path('cluster-1', views.spectral_cluster1, name='cluster_1'),
    path('cluster-2', views.spectral_cluster2, name='cluster_2'),
    path('cluster-3', views.spectral_cluster3, name='cluster_3'),
    path('cluster-4', views.spectral_cluster4, name='cluster_4'),
    path('cluster-5', views.spectral_cluster5, name='cluster_5'),
    path('reports', views.spectral_reports, name='reports'),
]