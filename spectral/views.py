from django.shortcuts import render, redirect
from django.conf import settings
from clustering.views import DATA_WEB
from django.http import HttpResponse
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import SpectralClustering
import pandas as pd
import json


spectral_cluster = pd.read_excel(settings.BASE_DIR / 'static/datasets/data_covid_bersihT.xlsx')
df2=spectral_cluster.copy()
sc = StandardScaler()
df_std = sc.fit_transform(df2)
Spectralclustering = SpectralClustering(n_clusters=6, eigen_solver=None, n_components=None,
                    random_state=74, n_init=10, gamma=1.0, affinity='nearest_neighbors', 
                    n_neighbors=10, eigen_tol=0.0, assign_labels='discretize',
                    degree=3, coef0=1, kernel_params=None, n_jobs=None, verbose=False).fit(df_std)
Spectral= Spectralclustering.labels_
spectral_cluster['Cluster_Spectral']=Spectral

cluster0 = spectral_cluster[spectral_cluster['Cluster_Spectral'] == 0]
cluster1 = spectral_cluster[spectral_cluster['Cluster_Spectral'] == 1]
cluster2 = spectral_cluster[spectral_cluster['Cluster_Spectral'] == 2]
cluster3 = spectral_cluster[spectral_cluster['Cluster_Spectral'] == 3]
cluster4 = spectral_cluster[spectral_cluster['Cluster_Spectral'] == 4]
cluster5 = spectral_cluster[spectral_cluster['Cluster_Spectral'] == 5]

def refresh_cluster(request):
    cluster = request.GET.get('cluster', '0')
    if cluster == '1':
        return redirect('spectral:cluster_1')
    elif cluster == '2':
        return redirect('spectral:cluster_2')
    elif cluster == '3':
        return redirect('spectral:cluster_3')
    elif cluster == '4':
        return redirect('spectral:cluster_4')
    elif cluster == '5':
        return redirect('spectral:cluster_5')
    else:
        return redirect('spectral:cluster_0')

def spectral_cluster0(request):
    title_data = cluster0.columns
    json_records = cluster0.reset_index().to_json(orient='records')
    data_csv = json.loads(json_records)
    context = {
        'title_page': 'Spectral Cluster 0',
        'title_data': title_data,
        'content_data': data_csv,
        'refresh_button_cluster_spectral': True,
        'data_query_param': 0,
        'cluster_view': True,
        'spectral_active': 'active',
        'upload_file': DATA_WEB['upload_file'],
    }
    return render(request, 'index.html', context)

def spectral_cluster1(request):
    title_data = cluster1.columns
    json_records = cluster1.reset_index().to_json(orient='records')
    data_csv = json.loads(json_records)
    context = {
        'title_page': 'Spectral Cluster 1',
        'title_data': title_data,
        'content_data': data_csv,
        'refresh_button_cluster_spectral': True,
        'data_query_param': 1,
        'cluster_view': True,
        'spectral_active': 'active',
        'upload_file': DATA_WEB['upload_file'],
    }
    return render(request, 'index.html', context)

def spectral_cluster2(request):
    title_data = cluster2.columns
    json_records = cluster2.reset_index().to_json(orient='records')
    data_csv = json.loads(json_records)
    context = {
        'title_page': 'Spectral Cluster 2',
        'title_data': title_data,
        'content_data': data_csv,
        'refresh_button_cluster_spectral': True,
        'data_query_param': 2,
        'cluster_view': True,
        'spectral_active': 'active',
        'upload_file': DATA_WEB['upload_file'],
    }
    return render(request, 'index.html', context)

def spectral_cluster3(request):
    title_data = cluster3.columns
    json_records = cluster3.reset_index().to_json(orient='records')
    data_csv = json.loads(json_records)
    context = {
        'title_page': 'Spectral Cluster 3',
        'title_data': title_data,
        'content_data': data_csv,
        'refresh_button_cluster_spectral': True,
        'data_query_param': 3,
        'cluster_view': True,
        'spectral_active': 'active',
        'upload_file': DATA_WEB['upload_file'],
    }
    return render(request, 'index.html', context)

def spectral_cluster4(request):
    title_data = cluster4.columns
    json_records = cluster4.reset_index().to_json(orient='records')
    data_csv = json.loads(json_records)
    context = {
        'title_page': 'Spectral Cluster 4',
        'title_data': title_data,
        'content_data': data_csv,
        'refresh_button_cluster_spectral': True,
        'data_query_param': 4,
        'cluster_view': True,
        'spectral_active': 'active',
        'upload_file': DATA_WEB['upload_file'],
    }
    return render(request, 'index.html', context)

def spectral_cluster5(request):
    title_data = cluster5.columns
    json_records = cluster5.reset_index().to_json(orient='records')
    data_csv = json.loads(json_records)
    context = {
        'title_page': 'Spectral Cluster 5',
        'title_data': title_data,
        'content_data': data_csv,
        'refresh_button_cluster_spectral': True,
        'data_query_param': 5,
        'cluster_view': True,
        'spectral_active': 'active',
        'upload_file': DATA_WEB['upload_file'],
    }
    return render(request, 'index.html', context)

def spectral_reports(request):
    cluster = request.GET.get('cluster', False)

    file_path = settings.BASE_DIR / 'static/datasets'
    file_name = 'data_covid_bersihT.xlsx'
    clustering = spectral_cluster

    if cluster:
        file_path =  settings.BASE_DIR / 'static/reports'

    if cluster == '0':
        file_name = 'Cluster_Spectral0.xlsx'
        clustering = cluster0
    elif cluster == '1':
        file_name = 'Cluster_Spectral1.xlsx'
        clustering = cluster1
    elif cluster == '2':
        file_name = 'Cluster_Spectral2.xlsx'
        clustering = cluster2
    elif cluster == '3':
        file_name = 'Cluster_Spectral3.xlsx'
        clustering = cluster3
    elif cluster == '4':
        file_name = 'Cluster_Spectral4.xlsx'
        clustering = cluster4
    elif cluster == '5':
        file_name = 'Cluster_Spectral5.xlsx'
        clustering = cluster5
    
    full_path = '{}/{}'.format(file_path, file_name)
    clustering.to_excel(full_path, index=False)
    file = open(full_path, 'rb').read()
    response = HttpResponse(file, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename=%s' % (file_name)
    return response