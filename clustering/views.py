from django.shortcuts import render
from django.conf import settings
from django.http import HttpResponse
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import SpectralClustering
from sklearn_extra.cluster import KMedoids
from pandas_profiling import ProfileReport
import pandas as pd
import json
import os
import tarfile


print('Starting Data Preparation ...')

# Data Web
DATA_WEB = {
    'upload_file': False
}

# Data diterima dari dinas
data_diterima = pd.read_excel (settings.BASE_DIR / 'static/datasets/__DATA COVID 2020.xlsx')

# Data sebelum dibersihkan
df = pd.read_excel (settings.BASE_DIR / 'static/datasets/__DATA_COVID_2020_v1B.xlsx')
df.dropna()
df['simptomatik'].replace({True:1,False:0},inplace=True)
df['simptomatik'].map({True: 1, False: 0})  
df['Asimptomatik'].replace({True:1,False:0},inplace=True)
df['BULAN_POSITIF'] = pd.DatetimeIndex(df['TANGGAL_POSITIF']).month
df['Tanggal_POSITIF'] = pd.DatetimeIndex(df['TANGGAL_POSITIF']).day
df['BULAN_SEMBUH'] = pd.DatetimeIndex(df['TANGGAL_SEMBUH']).month
df['Tanggal_SEMBUH'] = pd.DatetimeIndex(df['TANGGAL_SEMBUH']).day
df.fillna(0)

# Data setelah dibersihkan
df1=df.copy()
df1=df1.drop(['TANGGAL_POSITIF','TANGGAL_SEMBUH'], axis = 1)

def remove_outlier(df):
    Q1=df.quantile(0.25)
    Q3=df.quantile(0.75)
    IQR=Q3-Q1
    df_final=df[~(df>(Q1-(1.5*IQR)))|(df<(Q3+(1.5*IQR)))]
    return df_final
for x in range(2):
    df1=remove_outlier(df1)
    df1.dropna(axis=0,inplace=True)

df2 = df1.copy()
df2.drop(['Latitude','Longitude','KELURAHAN'],axis=1, inplace=True)
df2["UMUR"] = (df2["UMUR"] - df2["UMUR"].min()) / (df2["UMUR"].max()-df2["UMUR"].min())
df2["JENIS_KELAMIN"] = (df2["JENIS_KELAMIN"] - df2["JENIS_KELAMIN"].min()) / (df2["JENIS_KELAMIN"].max()-df2["JENIS_KELAMIN"].min())
df2["simptomatik"] = (df2["simptomatik"] - df2["simptomatik"].min()) / (df2["simptomatik"].max()-df2["simptomatik"].min())
df2["Asimptomatik"] = (df2["Asimptomatik"] - df2["Asimptomatik"].min()) / (df2["Asimptomatik"].max()-df2["Asimptomatik"].min())
df2["Kode Kelurahan"] = (df2["Kode Kelurahan"] - df2["Kode Kelurahan"].min()) / (df2["Kode Kelurahan"].max()-df2["Kode Kelurahan"].min())
df2["BULAN_POSITIF"] = (df2["BULAN_POSITIF"] - df2["BULAN_POSITIF"].min()) / (df2["BULAN_POSITIF"].max()-df2["BULAN_POSITIF"].min())
df2["Tanggal_POSITIF"] = (df2["Tanggal_POSITIF"] - df2["Tanggal_POSITIF"].min()) / (df2["Tanggal_POSITIF"].max()-df2["Tanggal_POSITIF"].min())
df2["BULAN_SEMBUH"] = (df2["BULAN_SEMBUH"] - df2["BULAN_SEMBUH"].min()) / (df2["BULAN_SEMBUH"].max()-df2["BULAN_SEMBUH"].min())
df2["Tanggal_SEMBUH"] = (df2["Tanggal_SEMBUH"] - df2["Tanggal_SEMBUH"].min()) / (df2["Tanggal_SEMBUH"].max()-df2["Tanggal_SEMBUH"].min())
df2.to_excel(settings.BASE_DIR / "static/datasets/data_covid_bersihT.xlsx",index=False)
sc = StandardScaler()
df_std = sc.fit_transform(df2)
print('Completed Data Preparation ...')

def index(request):
    context = {
        'title_page': 'Upload File',
        'upload_file': DATA_WEB['upload_file'],
    }
    return render(request, 'upload_file.html', context)

def real_data(request):
    if request.GET.get('upload_file', False):
        DATA_WEB['upload_file'] = True
    title_data = data_diterima.columns
    json_records = data_diterima.reset_index().to_json(orient='records')
    data_csv = json.loads(json_records)
    context = {
        'title_page': 'Data from Health Departement',
        'title_data': title_data,
        'content_data': data_csv,
        # 'refresh_button': False,
        'upload_file': DATA_WEB['upload_file'],
    }
    return render(request, 'index.html', context)

def uncleanup_data(request):
    title_data = df.columns
    json_records = df.reset_index().to_json(orient='records')
    data_csv = json.loads(json_records)
    context = {
        'title_page': 'Pre-Cleaned Data',
        'title_data': title_data,
        'content_data': data_csv,
        # 'refresh_button': False,
        'upload_file': DATA_WEB['upload_file'],
    }
    return render(request, 'index.html', context)

def cleanup_data(request):
    if 'hasil_Spectral' in df1.columns:
        df1.drop(['hasil_Spectral'], axis=1, inplace=True)

    if 'hasil_Kmedoid' in df1.columns:
        df1.drop(['hasil_Kmedoid'], axis=1, inplace=True)
    cluster_view = False
    refresh_button = False
    if request.GET.get('refresh_data', False) == '0':
        refresh_button = True
        if 'Latitude' in df1.columns: 
            df1.drop(['Latitude','Longitude','KELURAHAN'],axis=1, inplace=True)
        df1["UMUR"] = (df1["UMUR"] - df1["UMUR"].min()) / (df1["UMUR"].max()-df1["UMUR"].min())
        df1["JENIS_KELAMIN"] = (df1["JENIS_KELAMIN"] - df1["JENIS_KELAMIN"].min()) / (df1["JENIS_KELAMIN"].max()-df1["JENIS_KELAMIN"].min())
        df1["simptomatik"] = (df1["simptomatik"] - df1["simptomatik"].min()) / (df1["simptomatik"].max()-df1["simptomatik"].min())
        df1["Asimptomatik"] = (df1["Asimptomatik"] - df1["Asimptomatik"].min()) / (df1["Asimptomatik"].max()-df1["Asimptomatik"].min())
        df1["Kode Kelurahan"] = (df1["Kode Kelurahan"] - df1["Kode Kelurahan"].min()) / (df1["Kode Kelurahan"].max()-df1["Kode Kelurahan"].min())
        df1["BULAN_POSITIF"] = (df1["BULAN_POSITIF"] - df1["BULAN_POSITIF"].min()) / (df1["BULAN_POSITIF"].max()-df1["BULAN_POSITIF"].min())
        df1["Tanggal_POSITIF"] = (df1["Tanggal_POSITIF"] - df1["Tanggal_POSITIF"].min()) / (df1["Tanggal_POSITIF"].max()-df1["Tanggal_POSITIF"].min())
        df1["BULAN_SEMBUH"] = (df1["BULAN_SEMBUH"] - df1["BULAN_SEMBUH"].min()) / (df1["BULAN_SEMBUH"].max()-df1["BULAN_SEMBUH"].min())
        df1["Tanggal_SEMBUH"] = (df1["Tanggal_SEMBUH"] - df1["Tanggal_SEMBUH"].min()) / (df1["Tanggal_SEMBUH"].max()-df1["Tanggal_SEMBUH"].min())
    
    if request.GET.get('refresh_data', False) == '1':
        Spectralclustering = SpectralClustering(n_clusters=6, eigen_solver=None, n_components=None,
                            random_state=74, n_init=10, gamma=1.0, affinity='nearest_neighbors', 
                            n_neighbors=10, eigen_tol=0.0, assign_labels='discretize',
                            degree=3, coef0=1, kernel_params=None, n_jobs=None, verbose=False).fit(df_std)
        Kmedoidclustering = KMedoids(n_clusters=7, random_state=24,method='pam',).fit(df_std)
        Spectral= Spectralclustering.labels_
        df1['hasil_Spectral']=Spectral
        Kmedoid= Kmedoidclustering.labels_
        df1['hasil_Kmedoid']=Kmedoid

        # generate the file that will be made into a report
        df1.to_excel(settings.BASE_DIR / "static/reports/Spectral_VS_Kmedoid.xlsx",index=False)
        cluster_view = True
        refresh_button = True

    title_data = df1.columns
    json_records = df1.reset_index().to_json(orient='records')
    data_csv = json.loads(json_records)
    context = {
        'title_page': 'Cleaned Data',
        'title_data': title_data,
        'content_data': data_csv,
        'refresh_button': refresh_button,
        'cluster_view': cluster_view,
        'upload_file': DATA_WEB['upload_file'],
    }
    return render(request, 'index.html', context)

def excel_report(request):
    if not os.path.exists(settings.BASE_DIR / 'static/reports/Spectral_VS_Kmedoid.xlsx'):
        df1.to_excel(settings.BASE_DIR / "static/reports/Spectral_VS_Kmedoid.xlsx",index=False)
    file_name = 'Spectral_VS_Kmedoid.xlsx'
    file_path = settings.BASE_DIR / 'static/reports/Spectral_VS_Kmedoid.xlsx'
    file = open(file_path, 'rb').read()
    response = HttpResponse(file, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename=%s' % (file_name)
    return response

def html_report(request):
    profile = ProfileReport(df1, title="Spectral and Kmedoidt")
    profile.to_file(settings.BASE_DIR / "static/reports/Spectral_VS_Kmedoid.html")#hasil download
    file = open(settings.BASE_DIR / "static/reports/Spectral_VS_Kmedoid.html", 'rb').read()
    response = HttpResponse(file, content_type='text/html')
    response['Content-Disposition'] = 'attachment; filename=Spectral_VSKmedoid.html'
    return response
