from django.urls import path
from . import views

app_name = 'kmedoid'
urlpatterns = [
    path('refresh-cluster', views.refresh_cluster, name='kmedoid_refresh'),
    path('cluster-0', views.kmedoid_cluster0, name='cluster_0'),
    path('cluster-1', views.kmedoid_cluster1, name='cluster_1'),
    path('cluster-2', views.kmedoid_cluster2, name='cluster_2'),
    path('cluster-3', views.kmedoid_cluster3, name='cluster_3'),
    path('cluster-4', views.kmedoid_cluster4, name='cluster_4'),
    path('cluster-5', views.kmedoid_cluster5, name='cluster_5'),
    # path('cluster-6', views.kmedoid_cluster6, name='cluster_6'),
    path('reports', views.kmedoid_reports, name='reports'),
]