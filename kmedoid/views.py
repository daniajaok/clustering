from django.shortcuts import render, redirect
from django.http import HttpResponse
from clustering.views import DATA_WEB
from django.conf import settings
from sklearn.preprocessing import StandardScaler
from sklearn_extra.cluster import KMedoids
import pandas as pd
import json


kmedoid_cluster = pd.read_excel(settings.BASE_DIR / 'static/datasets/data_covid_bersihT.xlsx')
df2=kmedoid_cluster.copy()
sc = StandardScaler()
df_std = sc.fit_transform(df2)
Kmedoidclustering = KMedoids(n_clusters=6, random_state=24,method='pam',).fit(df_std)
Kmedoid = Kmedoidclustering.labels_
kmedoid_cluster['Cluster_Kmedoid']=Kmedoid

classtering0 = kmedoid_cluster[kmedoid_cluster['Cluster_Kmedoid'] == 0]
classtering1 = kmedoid_cluster[kmedoid_cluster['Cluster_Kmedoid'] == 1]
classtering2 = kmedoid_cluster[kmedoid_cluster['Cluster_Kmedoid'] == 2]
classtering3 = kmedoid_cluster[kmedoid_cluster['Cluster_Kmedoid'] == 3]
classtering4 = kmedoid_cluster[kmedoid_cluster['Cluster_Kmedoid'] == 4]
classtering5 = kmedoid_cluster[kmedoid_cluster['Cluster_Kmedoid'] == 5]


def refresh_cluster(request):
    cluster = request.GET.get('cluster', '0')
    if cluster == '1':
        return redirect('kmedoid:cluster_1')
    elif cluster == '2':
        return redirect('kmedoid:cluster_2')
    elif cluster == '3':
        return redirect('kmedoid:cluster_3')
    elif cluster == '4':
        return redirect('kmedoid:cluster_4')
    elif cluster == '5':
        return redirect('kmedoid:cluster_5')
    # elif cluster == '6':
    #     return redirect('kmedoid:cluster_6')
    else:
        return redirect('kmedoid:cluster_0')


def kmedoid_cluster0(request):
    title_data = classtering0.columns
    json_records = classtering0.reset_index().to_json(orient='records')
    data_csv = json.loads(json_records)
    context = {
        'title_page': 'Kmedoid Cluster 0',
        'title_data': title_data,
        'content_data': data_csv,
        'refresh_button_cluster_kmedoid': True,
        'data_query_param': 0,
        'cluster_view': True,
        'kmedoid_active': 'active',
        'upload_file': DATA_WEB['upload_file'],
    }
    return render(request, 'index.html', context)

def kmedoid_cluster1(request):
    title_data = classtering1.columns
    json_records = classtering1.reset_index().to_json(orient='records')
    data_csv = json.loads(json_records)
    context = {
        'title_page': 'Kmedoid Cluster 1',
        'title_data': title_data,
        'content_data': data_csv,
        'refresh_button_cluster_kmedoid': True,
        'data_query_param': 1,
        'cluster_view': True,
        'kmedoid_active': 'active',
        'upload_file': DATA_WEB['upload_file'],
    }
    return render(request, 'index.html', context)

def kmedoid_cluster2(request):
    title_data = classtering2.columns
    json_records = classtering2.reset_index().to_json(orient='records')
    data_csv = json.loads(json_records)
    context = {
        'title_page': 'Kmedoid Cluster 2',
        'title_data': title_data,
        'content_data': data_csv,
        'refresh_button_cluster_kmedoid': True,
        'data_query_param': 2,
        'cluster_view': True,
        'kmedoid_active': 'active',
        'upload_file': DATA_WEB['upload_file'],
    }
    return render(request, 'index.html', context)

def kmedoid_cluster3(request):
    title_data = classtering3.columns
    json_records = classtering3.reset_index().to_json(orient='records')
    data_csv = json.loads(json_records)
    context = {
        'title_page': 'Kmedoid Cluster 3',
        'title_data': title_data,
        'content_data': data_csv,
        'refresh_button_cluster_kmedoid': True,
        'data_query_param': 3,
        'cluster_view': True,
        'kmedoid_active': 'active',
        'upload_file': DATA_WEB['upload_file'],
    }
    return render(request, 'index.html', context)

def kmedoid_cluster4(request):
    title_data = classtering4.columns
    json_records = classtering4.reset_index().to_json(orient='records')
    data_csv = json.loads(json_records)
    context = {
        'title_page': 'Kmedoid Cluster 4',
        'title_data': title_data,
        'content_data': data_csv,
        'refresh_button_cluster_kmedoid': True,
        'data_query_param': 4,
        'cluster_view': True,
        'kmedoid_active': 'active',
        'upload_file': DATA_WEB['upload_file'],
    }
    return render(request, 'index.html', context)

def kmedoid_cluster5(request):
    title_data = classtering5.columns
    json_records = classtering5.reset_index().to_json(orient='records')
    data_csv = json.loads(json_records)
    context = {
        'title_page': 'Kmedoid Cluster 5',
        'title_data': title_data,
        'content_data': data_csv,
        'refresh_button_cluster_kmedoid': True,
        'data_query_param': 5,
        'cluster_view': True,
        'kmedoid_active': 'active',
        'upload_file': DATA_WEB['upload_file'],
    }
    return render(request, 'index.html', context)

# def kmedoid_cluster6(request):
#     title_data = classtering6.columns
#     json_records = classtering6.reset_index().to_json(orient='records')
#     data_csv = json.loads(json_records)
#     context = {
#         'title_page': 'Kmedoid Cluster 6',
#         'title_data': title_data,
#         'content_data': data_csv,
#         'refresh_button_cluster_kmedoid': True,
#         'data_query_param': 6,
#         'cluster_view': True,
#         'kmedoid_active': 'active',
#         'upload_file': DATA_WEB['upload_file'],
#     }
#     return render(request, 'index.html', context)

def kmedoid_reports(request):
    cluster = request.GET.get('cluster', False)

    file_path = settings.BASE_DIR / 'static/datasets'
    file_name = 'data_covid_bersihT.xlsx'
    clustering = kmedoid_cluster

    if cluster:
        file_path =  settings.BASE_DIR / 'static/reports'

    if cluster == '0':
        file_name = 'Cluster_Kmedoid0.xlsx'
        clustering = classtering0
    elif cluster == '1':
        file_name = 'Cluster_Kmedoid1.xlsx'
        clustering = classtering1
    elif cluster == '2':
        file_name = 'Cluster_Kmedoid2.xlsx'
        clustering = classtering2
    elif cluster == '3':
        file_name = 'Cluster_Kmedoid3.xlsx'
        clustering = classtering3
    elif cluster == '4':
        file_name = 'Cluster_Kmedoid4.xlsx'
        clustering = classtering4
    elif cluster == '5':
        print('DI SINII')
        file_name = 'Cluster_Kmedoid5.xlsx'
        clustering = classtering5
    # elif cluster == '6':
    #     file_name = 'Cluster_Kmedoid6.xlsx'
    #     clustering = classtering6
    
    full_path = '{}/{}'.format(file_path, file_name)
    clustering.to_excel(full_path, index=False)
    file = open(full_path, 'rb').read()
    response = HttpResponse(file, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename=%s' % (file_name)
    return response