const sidenavDropdownLinks = document.querySelectorAll('.sidenav__dropdown > a')

sidenavDropdownLinks.forEach(sidenavDropdownLink => {
    sidenavDropdownLink.addEventListener('click', (e) => {
        e.preventDefault()
    })
})

const navDropdownLinks = document.querySelectorAll('.navbar__dropdown > a')
const reportsDropdowns = document.querySelectorAll('.reports__dropdown')
const spectralDropdowns = document.querySelectorAll('.spectral__dropdown')
const kmedoidDropdowns = document.querySelectorAll('.kmedoid__dropdown')
const sidenavDropdowns = [reportsDropdowns, spectralDropdowns, kmedoidDropdowns]

const deleteSidenavDropdownActive = () => {
    sidenavDropdowns.forEach(sidenavDropdown => {
        sidenavDropdown.forEach(dropdown => {
            dropdown.classList.remove('active')
        })
    })
}

navDropdownLinks.forEach((navDropdownLink, index) => {
    navDropdownLink.addEventListener('click', (e) => {
        e.preventDefault()
        deleteSidenavDropdownActive();
        sidenavDropdowns[index].forEach(dropdown => {
            dropdown.classList.add('active')
        })
    })
})

